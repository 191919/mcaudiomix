#include "stdafx.h"
#include "MCAudioMix.h"

// {FBE4109F-DA86-449B-8559-EC56BA9F4884}
DEFINE_GUID(CLSID_MCAudioMix,
	0xfbe4109f, 0xda86, 0x449b, 0x85, 0x59, 0xec, 0x56, 0xba, 0x9f, 0x48, 0x84);

static const WCHAR g_wszFilterName[] = L"MovieCam Audio Mixer";
static const TCHAR g_szFilterName[] = TEXT("MovieCam Audio Mixer");

AMOVIESETUP_FILTER FilterInfo =
{
    &CLSID_MCAudioMix,	// CLSID
	g_wszFilterName,	// Name
	MERIT_DO_NOT_USE,	// Merit
	0,					// Number of AMOVIESETUP_PIN structs
	NULL				// Pin registration information.
};

CFactoryTemplate g_Templates[] =
{
    {
		g_wszFilterName,				// Name
		&CLSID_MCAudioMix,				// CLSID
		CMCAudioMix::CreateInstance, // Method to create an instance of MyComponent
		NULL,							// Initialization function
		&FilterInfo						// Set-up information (for filters)
    }
};

int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);

STDAPI DllRegisterServer(void)
{
    return AMovieDllRegisterServer2(TRUE);
}

STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2(FALSE);
}

EXTERN_C BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);
BOOL APIENTRY DllMain(HANDLE hModule,DWORD dwReason,LPVOID lpReserved)
{
    return DllEntryPoint((HINSTANCE)hModule,(ULONG)dwReason,lpReserved);
}

CUnknown* WINAPI CMCAudioMix::CreateInstance(LPUNKNOWN pUnk, HRESULT *pHr)
{
    ASSERT(pHr);

	CMCAudioMix *pFilter = new CMCAudioMix(g_szFilterName, pUnk, CLSID_MCAudioMix, pHr);
    if (pFilter == NULL)
    {
		if(NULL != pHr)
		{
			*pHr = E_OUTOFMEMORY;
		}
    }
    return pFilter;
}

CMCAudioMix::CMCAudioMix(LPCTSTR lpszName, LPUNKNOWN pUnk, REFCLSID clsid, HRESULT *phr, int iMaxInPinAmt) :
			CBaseFilter((LPTSTR)lpszName, pUnk,  &m_csFilter, clsid, phr),
			m_pOutPin(NULL),
			m_lstInPins(NAME("Tee Output Pins list")),
			m_iMaxInPinsAmt(iMaxInPinAmt),
			m_iNextInPinNum(0),
			m_iInPinsAmt(0),
			m_pCurMediaType(NULL),
			m_rtSpan(0)
{
    ASSERT(phr);

	HRESULT hr = NOERROR;
	m_pOutPin = new CMCAudioMixOutputPin(NAME("Output"), this, &hr, L"Output");
	ASSERT(NULL != m_pOutPin);
	ClearInPinsList();
	CreateNextInPin();
}

CMCAudioMix::~CMCAudioMix()
{
	ClearInPinsList();
	SafeDelete(m_pOutPin);
}

HRESULT CMCAudioMix::CheckInputType(const CMediaType* mtIn)
{
	HRESULT hr = E_INVALIDARG;

	if (NULL == m_pCurMediaType)
	{
		if (mtIn->majortype == MEDIATYPE_Audio &&
			(mtIn->subtype == MEDIASUBTYPE_WAVE ||
			mtIn->subtype == MEDIASUBTYPE_PCM ) &&
			mtIn->formattype == FORMAT_WaveFormatEx)
		{
			hr = S_OK;
		}
		if (SUCCEEDED(hr))
		{
			m_pCurMediaType = new CMediaType(*mtIn, &hr);
			ASSERT(NULL != m_pCurMediaType);

			const WAVEFORMATEX* pwfx = (const WAVEFORMATEX*) m_pCurMediaType->Format();
			const double dBufferTime = (OUTPUT_BUFFER_SIZE) / (pwfx->nSamplesPerSec * (pwfx->wBitsPerSample / 8) * pwfx->nChannels);
			m_rtSpan = (REFERENCE_TIME) (dBufferTime * UNITS);
		}
	}
	else
	{
		if (mtIn->majortype == MEDIATYPE_Audio &&
			(mtIn->subtype == MEDIASUBTYPE_WAVE ||
			mtIn->subtype == MEDIASUBTYPE_PCM ) &&
			mtIn->formattype == FORMAT_WaveFormatEx)
		{
			WAVEFORMATEX* pwfxCur = (WAVEFORMATEX*) m_pCurMediaType->Format();
			WAVEFORMATEX* pwfxIn = (WAVEFORMATEX*) mtIn->Format();

			if (pwfxIn->cbSize == pwfxCur->cbSize &&
				pwfxIn->wFormatTag == pwfxCur->wFormatTag &&
				pwfxIn->nSamplesPerSec == pwfxCur->nSamplesPerSec &&
				pwfxIn->nChannels == pwfxCur->nChannels &&
				pwfxIn->wBitsPerSample == pwfxCur->wBitsPerSample)
			{
				hr = S_OK;
			}
		}
	}

	return hr;
}

//HRESULT CMCAudioMix::CheckTransform(const CMediaType* mtIn, const CMediaType* mtOut)
//{
//	CheckPointer(mtIn, E_POINTER);
//	CheckPointer(mtOut, E_POINTER);
//
//	// formats must be big enough
//	if(mtIn->FormatLength() < sizeof(WAVEFORMATEX) ||
//		mtOut->FormatLength() < sizeof(WAVEFORMATEX))
//		return E_INVALIDARG;
//
//	WAVEFORMATEX *pFmtIn = (WAVEFORMATEX *)mtIn->Format();
//	WAVEFORMATEX *pFmtOut = (WAVEFORMATEX *)mtOut->Format();
//
//	int iCmpResult = memcmp(pFmtIn, pFmtOut, sizeof(WAVEFORMATEX));
//	if(0 == iCmpResult)
//		return NOERROR;
//
//	return E_INVALIDARG;
//}
//
//HRESULT CMCAudioMix::DecideBufferSize(IMemAllocator * pAlloc, ALLOCATOR_PROPERTIES *pProp)
//{
//	CheckPointer(pAlloc,E_POINTER);
//	CheckPointer(pProp,E_POINTER);
//
//	// Is the input pin connected
//	if(!m_pInput->IsConnected())
//	{
//		return E_UNEXPECTED;
//	}
//
//	HRESULT hr = NOERROR;
//	ALLOCATOR_PROPERTIES InProp;
//	hr = ((CMCAudioMixInputPin *)m_pInput)->PeekAllocator()->GetProperties(&InProp);
//	if(FAILED(hr))
//	{
//		return hr;
//	}
//	hr = pAlloc->SetProperties(&InProp, pProp);
//	if(FAILED(hr))
//	{
//		return hr;
//	}
//
//	int iCmpResult = memcmp(&InProp, pProp, sizeof(ALLOCATOR_PROPERTIES));
//	if(0 != iCmpResult)
//	{
//		return E_UNEXPECTED;
//	}
//
//	return NOERROR;
//}
//

HRESULT CMCAudioMix::GetMediaType(int iPosition, CMediaType *pMediaType)
{
	CheckPointer(pMediaType,E_POINTER);

	if (NULL == m_pCurMediaType)
	{
		return E_UNEXPECTED;
	}

	if (iPosition < 0)
	{
		return E_INVALIDARG;
	}

	if (iPosition > 0)
	{
		return VFW_S_NO_MORE_ITEMS;
	}

	*pMediaType = *m_pCurMediaType;
	return NOERROR;
}


int CMCAudioMix::GetPinCount()
{
	return m_iInPinsAmt + 1;
}

CBasePin *CMCAudioMix::GetPin(int n)
{
	if (n < 0)
		return NULL;

	if (n == 0)
	{
		ASSERT(NULL != m_pOutPin);
		return m_pOutPin;
	}

	return GetInPin(n - 1);
}

void CMCAudioMix::ClearInPinsList()
{
	POSITION pos = m_lstInPins.GetHeadPosition();

	while (pos)
	{
		CMCAudioMixInputPin *pPin = m_lstInPins.GetNext(pos);
		pPin->Release();
	}

	m_iInPinsAmt = 0;
	m_lstInPins.RemoveAll();

}

CMCAudioMixInputPin *CMCAudioMix::GetInPin(int n)
{
	if (n >= m_iInPinsAmt)
		return NULL;

	POSITION pos = m_lstInPins.GetHeadPosition();

	CMCAudioMixInputPin *pPin = NULL;

	while (n >= 0)
	{
		pPin = m_lstInPins.GetNext(pos);
		n--;
	}

	return pPin;
}

HRESULT CMCAudioMix::CreateNextInPin()
{
	HRESULT hr = NOERROR;

	int n = GetFreeInPinsAmt();
	ASSERT(n <= 1);
	if (n == 1 || m_iInPinsAmt == m_iMaxInPinsAmt)
		return NOERROR;

	WCHAR wszbuf[128] = { 0 };
	swprintf(wszbuf, L"Input %03d\0", m_iNextInPinNum);

	CMCAudioMixInputPin *pPin = new CMCAudioMixInputPin(NAME("Input"), this, &hr, (LPCWSTR)wszbuf);

	if (FAILED(hr) || pPin == NULL)
	{
		SafeDelete(pPin);
		return hr;
	}

	pPin->AddRef();
	m_iInPinsAmt++;
	m_iNextInPinNum++;
	m_lstInPins.AddTail(pPin);
	IncrementPinVersion();

	return hr;
}

void CMCAudioMix::DeleteInPin(CMCAudioMixInputPin *pInPin)
{
	if (NULL == pInPin)
		return;

	POSITION pos = m_lstInPins.GetHeadPosition();

	while (pos)
	{
		POSITION posold = pos;
		CMCAudioMixInputPin *pPin = m_lstInPins.GetNext(pos);

		if (pInPin == pPin)
		{
			m_lstInPins.Remove(posold);
			SafeDelete(pInPin);

			m_iInPinsAmt--;
			IncrementPinVersion();
			break;
		}
	}
}

int CMCAudioMix::GetFreeInPinsAmt()
{
	int n = 0;
	POSITION pos = m_lstInPins.GetHeadPosition();

	while (pos)
	{
		CMCAudioMixInputPin *pPin = m_lstInPins.GetNext(pos);

		if (pPin && !pPin->IsConnected())
			n++;
	}

	return n;
}