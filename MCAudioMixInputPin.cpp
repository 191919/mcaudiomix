#include "StdAfx.h"

#include "MCAudioMix.h"
#include "MCAudioMixInputPin.h"

CMCAudioMixInputPin::CMCAudioMixInputPin(LPCTSTR lpszObjName, CMCAudioMix *pFilter, HRESULT *phr, LPCWSTR pName) :
	CBaseInputPin((LPTSTR)lpszObjName, pFilter, &pFilter->m_csFilter, phr, pName),
		m_pFilter(pFilter),
		m_cOurRef(0), m_Chunks(TEXT("Audio Sample Chunks"))
{

}

CMCAudioMixInputPin::~CMCAudioMixInputPin()
{
	for (POSITION pos = m_Chunks.GetHeadPosition(); pos != NULL;)
	{
		CAudioSampleChunk* pChunk = m_Chunks.GetNext(pos);
		delete pChunk;
	}
}

STDMETHODIMP_(ULONG) CMCAudioMixInputPin::NonDelegatingAddRef()
{
	CAutoLock lock_it(m_pLock);

#ifdef DEBUG
	m_cRef++;
	ASSERT(m_cRef > 0);
#endif

	m_cOurRef++;
	ASSERT(m_cOurRef > 0);
	return m_cOurRef;
}

STDMETHODIMP_(ULONG) CMCAudioMixInputPin::NonDelegatingRelease()
{
	CAutoLock lock_it(m_pLock);

#ifdef DEBUG
	m_cRef--;
	ASSERT(m_cRef >= 0);
#endif

	m_cOurRef--;
	ASSERT(m_cOurRef >= 0);

	if (m_cOurRef <= 1)
	{
		int n = 2;
		if (m_cOurRef == 1)
		{
			n = m_pFilter->GetFreeInPinsAmt();
		}

		if (n >= 2)
		{
			m_cOurRef = 0;
#ifdef DEBUG
			m_cRef = 0;
#endif
			m_pFilter->DeleteInPin(this);
			return (ULONG) 0;
		}
	}

	return (ULONG) m_cOurRef;
}

HRESULT CMCAudioMixInputPin::CompleteConnect(IPin *pReceivePin)
{
	CAutoLock lock_it(m_pLock);
	ASSERT(m_Connected == pReceivePin);
	HRESULT hr = NOERROR;

	CBaseInputPin::CompleteConnect(pReceivePin);

	hr = m_pFilter->CreateNextInPin();
	return hr;
}

HRESULT CMCAudioMixInputPin::BeginFlush(void)
{
	HRESULT hr = NOERROR;

	return hr;
}

HRESULT CMCAudioMixInputPin::EndFlush(void)
{
	HRESULT hr = NOERROR;

	return hr;
}

HRESULT CMCAudioMixInputPin::Receive(IMediaSample *pSample)
{
	HRESULT hr = __super::Receive(pSample);

	BYTE* pData;
	pSample->GetPointer(&pData);
	LONG lActualSize = pSample->GetActualDataLength();

#ifdef _DEBUG
	char s[256];
	wsprintfA(s, "%s: tid=%d pData=%p lSize=%ld actual=%ld\r\n", __FUNCTION__, (int) GetCurrentThreadId(), pData, lDataSize, lActualSize);
	OutputDebugStringA(s);
#endif

	CAudioSampleChunk* pChunk = new CAudioSampleChunk(pData, lActualSize);

	m_csChunks.Lock();
	m_Chunks.AddTail(pChunk);
	m_csChunks.Unlock();

	return hr;
}

//HRESULT CMCAudioMixInputPin::GetMediaType(int iPosition, CMediaType *pMediaType)
//{
//	HRESULT hr = NOERROR;
//
//	return hr;
//}

HRESULT CMCAudioMixInputPin::CheckMediaType(const CMediaType *pmt)
{
	return m_pFilter->CheckInputType(pmt);
}
