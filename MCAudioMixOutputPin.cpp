#include "StdAfx.h"
#include "MCAudioMix.h"
#include "MCAudioMixOutputPin.h"

CMCAudioMixOutputPin::CMCAudioMixOutputPin(LPCTSTR pObjName, CMCAudioMix *pFilter, HRESULT *phr, LPCWSTR pName) :
	CBaseOutputPin((LPTSTR)pObjName, pFilter, &pFilter->m_csFilter, phr, pName),
	m_pFilter(pFilter)
{
	ASSERT(pFilter);
}

CMCAudioMixOutputPin::~CMCAudioMixOutputPin()
{
}

HRESULT CMCAudioMixOutputPin::DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES* pProperties)
{
	pProperties->cbBuffer = OUTPUT_BUFFER_SIZE;
	pProperties->cBuffers = 2;

	pProperties->cBuffers /= 2;
	if (pProperties->cBuffers < 1)
		pProperties->cBuffers = 1;

	ALLOCATOR_PROPERTIES Actual;
	HRESULT hr = pAlloc->SetProperties(pProperties, &Actual);
	if (Actual.cbBuffer < pProperties->cbBuffer)
	{
		return E_FAIL;
	}

	return NOERROR;
}

HRESULT	CMCAudioMixOutputPin::GetMediaType(int iPosition, CMediaType *pMediaType)
{
	CAutoLock lock_it(m_pLock);

	return m_pFilter->GetMediaType(iPosition, pMediaType);
}

HRESULT CMCAudioMixOutputPin::CheckMediaType(const CMediaType *pmt)
{
	CAutoLock lock_it(m_pLock);

	return m_pFilter->CheckInputType(pmt);
}

HRESULT CMCAudioMixOutputPin::DeliverPacket(BYTE* p, INT_PTR nBytes, REFERENCE_TIME* prtStart, REFERENCE_TIME* prtEnd)
{
	HRESULT hr;

	if (nBytes == 0)
	{
		return S_OK;
	}

	do
	{
		IMediaSample* pSample = NULL;
		if (S_OK != (hr = GetDeliveryBuffer(&pSample, NULL, NULL, 0)))
		{
			break;
		}

		if (nBytes > pSample->GetSize())
		{
			pSample->Release();

			ALLOCATOR_PROPERTIES props, actual;
			if (S_OK != (hr = m_pAllocator->GetProperties(&props)))
			{
				break;
			}
			props.cbBuffer = nBytes * 3 / 2;

			if (props.cBuffers > 1)
			{
				if (S_OK != (hr = __super::DeliverBeginFlush()))
				{
					break;
				}
				if (S_OK != (hr = __super::DeliverEndFlush()))
				{
					break;
				}
			}

			if (S_OK != (hr = m_pAllocator->Decommit()))
			{
				break;
			}
			if (S_OK != (hr = m_pAllocator->SetProperties(&props, &actual)))
			{
				break;
			}
			if (S_OK != (hr = m_pAllocator->Commit()))
			{
				break;
			}
			if (S_OK != (hr = GetDeliveryBuffer(&pSample, NULL, NULL, 0)))
			{
				break;
			}
		}

		BYTE* pData = NULL;
		if (S_OK != (hr = pSample->GetPointer(&pData)) || !pData)
		{
			break;
		}
		memcpy(pData, p, nBytes);
		if (S_OK != (hr = pSample->SetActualDataLength(nBytes)))
		{
			break;
		}
		if (S_OK != (hr = pSample->SetTime(prtStart, prtEnd)))
		{
			break;
		}
		if (S_OK != (hr = pSample->SetMediaTime(NULL, NULL)))
		{
			break;
		}
		if (S_OK != (hr = pSample->SetDiscontinuity(FALSE)))
		{
			break;
		}
		if (S_OK != (hr = pSample->SetSyncPoint(FALSE)))
		{
			break;
		}
		if (S_OK != (hr = pSample->SetPreroll(FALSE)))
		{
			break;
		}
		if (S_OK != (hr = Deliver(pSample)))
		{
			break;
		}

		pSample->Release();
	} while(FALSE);

	return hr;
}

DWORD CMCAudioMixOutputPin::ThreadProc()
{
	REFERENCE_TIME rt = 0;

	for (;;)
	{
		Sleep(2);

		DWORD cmd;
		if (CheckRequest(&cmd))
		{
			m_hThread = NULL;
			cmd = GetRequest();
			Reply(S_OK);
			return 0;
		}

		BOOL bAllPinsHaveEnoughData = TRUE;
		POSITION pos = m_pFilter->m_lstInPins.GetHeadPosition();
		while (pos)
		{
			CMCAudioMixInputPin *pPin = m_pFilter->m_lstInPins.GetNext(pos);

			pPin->m_csChunks.Lock();
			LONG lRemainedDataSize = pPin->GetChunksTotalRemainedSize(OUTPUT_BUFFER_SIZE);
			pPin->m_csChunks.Unlock();

			if (pPin->IsConnected() && lRemainedDataSize < OUTPUT_BUFFER_SIZE)
			{
				bAllPinsHaveEnoughData = FALSE;
				break;
			}
		}
		if (!bAllPinsHaveEnoughData)
		{
			continue;
		}

		// Consume buffers of each pin
		int nPins = m_pFilter->m_lstInPins.GetCount() - 1;
		BYTE** pDataArray = new BYTE*[nPins];

		pos = m_pFilter->m_lstInPins.GetHeadPosition();
		int i = 0;
		while (pos)
		{
			CMCAudioMixInputPin *pPin = m_pFilter->m_lstInPins.GetNext(pos);
			if (!pPin->IsConnected()) continue;

			pPin->m_csChunks.Lock();
			pDataArray[i] = pPin->ConsumeDataFromChunks(OUTPUT_BUFFER_SIZE);
			pPin->m_csChunks.Unlock();
			++i;
		}

		// Mix
		BYTE* pResult = new BYTE[OUTPUT_BUFFER_SIZE];
		SHORT* psResult = (SHORT*) pResult;
		for (int s = 0; s < OUTPUT_BUFFER_SIZE / sizeof(SHORT); ++s)
		{
			int sample = 0;
			for (int i = 0; i < nPins; ++i)
			{
				const SHORT* sp = (SHORT*) pDataArray[i];
				sample += sp[s];
			}
			if (sample < -32767) sample = -32767;
			if (sample > 32767) sample = 32767;
			psResult[s] = (SHORT) sample;
		}

		for (int i = 0; i < nPins; ++i)
		{
			delete[] pDataArray[i];
		}
		delete[] pDataArray;

		// Calculate timestamp
		REFERENCE_TIME rtStart = rt;
		REFERENCE_TIME rtEnd = rtStart + m_pFilter->m_rtSpan;
		rt = rtEnd;
		
		// Deliver
		DeliverPacket(pResult, OUTPUT_BUFFER_SIZE, &rtStart, &rtEnd);
		delete[] pResult;
	}
	return 0;
}