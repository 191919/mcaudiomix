#pragma once

class CMCAudioMix;
class CMCAudioMixOutputPin;

class CAudioSampleChunk
{
	BYTE* m_pData;
	LONG m_lLength;
	LONG m_lConsumed;

public:
	CAudioSampleChunk(BYTE* pData, LONG lLength)
	{
		m_pData = new BYTE[lLength];
		memcpy(m_pData, pData, lLength);

		m_lLength = lLength;
		m_lConsumed = 0;
	}

	~CAudioSampleChunk()
	{
		delete[] m_pData;
	}

	LONG GetLength() const
	{
		return m_lLength;
	}

	BYTE* GetUnconsumedData() const
	{
		return m_pData + m_lConsumed;
	}

	BYTE* GetData() const
	{
		return m_pData;
	}

	LONG GetConsumed() const
	{
		return m_lConsumed;
	}

	void SetConsumed(LONG lConsumed)
	{
		m_lConsumed = lConsumed;
	}

	LONG AddConsumed(LONG lIncrement)
	{
		m_lConsumed += lIncrement;
		ASSERT(m_lConsumed <= m_lLength);
		return m_lConsumed;
	}

	BOOL IsDrained() const
	{
		return (m_lConsumed == m_lLength);
	}
};

// Class CMCAudioMixInputPin
class CMCAudioMixInputPin : public CBaseInputPin
{
	friend class CMCAudioMixOutputPin;
	typedef CGenericList<CAudioSampleChunk> SampleChunks;
public:
	DECLARE_IUNKNOWN;
	CMCAudioMixInputPin(LPCTSTR lpszObjName, CMCAudioMix *pFilter, HRESULT *phr, LPCWSTR pName);
	~CMCAudioMixInputPin();

	BOOL IsConnected() const { return m_Connected != NULL; }
	IMemAllocator* PeekAllocator() const { return m_pAllocator; }

	HRESULT CompleteConnect(IPin *pReceivePin);
	STDMETHODIMP_(ULONG) NonDelegatingAddRef();
	STDMETHODIMP_(ULONG) NonDelegatingRelease();

	STDMETHODIMP BeginFlush(void);
	STDMETHODIMP EndFlush(void);
	STDMETHODIMP Receive(IMediaSample *pSample);
	//HRESULT GetMediaType(int iPosition, CMediaType *pMediaType);
	HRESULT CheckMediaType(const CMediaType *pmt);

protected:
	CMCAudioMix *m_pFilter;
	LONG m_cOurRef;
	SampleChunks m_Chunks;
	CCritSec m_csChunks;

	BYTE* ConsumeDataFromChunks(LONG lTotal)
	{
		BYTE* pData = new BYTE[lTotal];

		LONG lLeft = lTotal;
		LONG lAsked = 0;

		for (POSITION pos = m_Chunks.GetHeadPosition(); pos != NULL && lLeft > 0;)
		{
			CAudioSampleChunk* pChunk = m_Chunks.GetNext(pos);
			LONG lRemained = pChunk->GetLength() - pChunk->GetConsumed();
			if (lRemained <= lLeft)
			{
				memcpy(pData + lAsked, pChunk->GetUnconsumedData(), lRemained);
				pChunk->AddConsumed(lRemained);
				lAsked += lRemained;
				lLeft -= lRemained;
			}
			else
			{
				memcpy(pData + lAsked, pChunk->GetUnconsumedData(), lLeft);
				pChunk->AddConsumed(lLeft);
				lAsked += lLeft;
				lLeft = 0;
			}
		}

		for (;;)
		{
			CAudioSampleChunk* pChunk = m_Chunks.GetHead();
			if (!pChunk || !pChunk->IsDrained())
			{
				break;
			}
			
			m_Chunks.RemoveHead();
			delete pChunk;
		}

		return pData;
	}

	LONG GetChunksTotalRemainedSize(LONG lMinimalRequired)
	{
		LONG lTotal = 0;

		for (POSITION pos = m_Chunks.GetHeadPosition(); pos != NULL;)
		{
			CAudioSampleChunk* pChunk = m_Chunks.GetNext(pos);
			LONG lRemained = pChunk->GetLength() - pChunk->GetConsumed();
			lTotal += lRemained;
			if (lMinimalRequired > 0 && lTotal >= lMinimalRequired) break;
		}
		return lTotal;
	}
};