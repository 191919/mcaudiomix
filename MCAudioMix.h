#pragma once

#include "stdafx.h"
#include "MCAudioMixOutputPin.h"
#include "MCAudioMixInputPin.h"

#define OUTPUT_BUFFER_SIZE (40*1024) // about 1/4 seconds

#define SafeDelete(p) { if(p) delete (p); p=NULL; }
#define SafeDeleteArray(p) { if(p) { delete[] (p); (p)=NULL; } }

EXTERN_C const GUID CLSID_MCAudioMix;

class CMCAudioMix : public CBaseFilter
{
	friend class CMCAudioMixInputPin;
	friend class CMCAudioMixOutputPin;
	typedef CGenericList<CMCAudioMixInputPin> InPinsList;
public:
    static CUnknown* WINAPI CreateInstance(LPUNKNOWN pUnk, HRESULT *pHr);
	DECLARE_IUNKNOWN;
	
	CMCAudioMix(LPCTSTR lpszName, LPUNKNOWN pUnk, REFCLSID clsid, HRESULT* phr, int iMaxInPinAmt = 32);
	~CMCAudioMix();

	HRESULT CheckInputType(const CMediaType* mtIn);
	HRESULT GetMediaType(int iPosition, CMediaType *pMediaType);
	int GetPinCount(void);
	CBasePin* GetPin(int n);

protected:
	void ClearInPinsList();
	CMCAudioMixInputPin* GetInPin(int n);
	HRESULT CreateNextInPin();
	void DeleteInPin(CMCAudioMixInputPin *pInPin);
	int GetFreeInPinsAmt();

protected:
	CCritSec m_csFilter;
	CCritSec m_csReceive;

	CMCAudioMixOutputPin* m_pOutPin;
	InPinsList m_lstInPins;
	int m_iMaxInPinsAmt;
	int m_iInPinsAmt;
	int m_iNextInPinNum;

	CMediaType* m_pCurMediaType;
	REFERENCE_TIME m_rtSpan;
};
