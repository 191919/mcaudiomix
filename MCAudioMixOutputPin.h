#pragma once

class CMCAudioMixOutputPin : public CBaseOutputPin, protected CAMThread
{
	friend class CMCAudioMix;
	CMCAudioMix *m_pFilter;
	enum { CMD_EXIT };
public:
	CMCAudioMixOutputPin(LPCTSTR pObjName, CMCAudioMix *pFilter, HRESULT *phr, LPCWSTR pName);
	~CMCAudioMixOutputPin();

	HRESULT CheckMediaType(const CMediaType *pmt);
	HRESULT	GetMediaType(int iPosition, CMediaType *pMediaType);
	HRESULT DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES* pProperties);
	HRESULT DeliverPacket(BYTE* p, INT_PTR nBytes, REFERENCE_TIME* prtStart, REFERENCE_TIME* prtEnd);

	HRESULT Active()
	{
		CAutoLock cAutoLock(m_pLock);
		if (m_Connected)
		{
			Create();
		}
		return __super::Active();
	}

	HRESULT Inactive()
	{
		CAutoLock cAutoLock(m_pLock);

		if (ThreadExists())
		{
			CallWorker(CMD_EXIT);
		}

		return __super::Inactive();
	}

	HANDLE GetThreadHandle()
	{
		ASSERT(m_hThread != NULL);
		return m_hThread;
	}
	
	void SetThreadPriority(int nPriority)
	{
		if (m_hThread)
		{
			::SetThreadPriority(m_hThread, nPriority);
		}
	}

	DWORD ThreadProc();
}; 
